<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int|null $id_u
 * @property string|null $name
 * @property float|null $cost
 * @property string $date_begin
 * @property string $date_end
 *
 * @property Users $u
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_u'], 'integer'],
            [['cost'], 'number'],
            [['date_begin', 'date_end'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['id_u'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_u' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_u' => 'Пользователь',
            'name' => 'Название',
            'cost' => 'Стоимость',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата сдачи',
        ];
    }

    /**
     * Gets query for [[U]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getU()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_u']);
    }
}
