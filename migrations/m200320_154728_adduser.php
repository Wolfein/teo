<?php

use yii\db\Migration;

/**
 * Class m200320_154728_adduser
 */
class m200320_154728_adduser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', [
            'FIO' => 'admin',
            'Login' => 'admin',
            'Pass' => 'admin',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200320_154728_adduser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200320_154728_adduser cannot be reverted.\n";

        return false;
    }
    */
}
