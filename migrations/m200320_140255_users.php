<?php

use yii\db\Migration;

/**
 * Class m200320_140255_users
 */
class m200320_140255_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'FIO' => $this->string(128)->notNull(),
            'Login' => $this->string(32)->notNull(),
            'Pass' => $this->string(32)->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200320_140255_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200320_140255_users cannot be reverted.\n";

        return false;
    }
    */
}
