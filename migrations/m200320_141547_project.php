<?php

use yii\db\Migration;

/**
 * Class m200320_141547_project
 */
class m200320_141547_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'id_u' => $this->integer(11),
            'name' => $this->string(255),
            'cost' => $this->float(),
            'date_begin' => $this->timestamp(),
            'date_end' => $this->timestamp(),

        ], $tableOptions);

        $this->addForeignKey(
            'id_u',
            'projects',
            'id_u',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200320_141547_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200320_141547_project cannot be reverted.\n";

        return false;
    }
    */
}
